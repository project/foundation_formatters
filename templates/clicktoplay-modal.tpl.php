<div class="click-to-play click-to-play-modal" id="click-to-play-wrapper-<?php print $id; ?>" >
  <a href="#" data-reveal-id="click-to-play-<?php print $id; ?>" id="click-to-play-link-<?php print $id; ?>" data-oembed="<?php print $content; ?>">
    <?php print $placeholder; ?>
    <div class="<?php print $play_class; ?>" class=""></div>
  </a>
  <div id="click-to-play-<?php print $id; ?>" class="reveal-modal large" data-reveal>
    <h3 class="video-spacer">&nbsp;</h3>
    <div class="click-to-play-video-wrapper"><div class="flex-video"></div></div>
    <a class="close-reveal-modal">&#215;</a>
  </div>
</div>

<script type="text/javascript">
jQuery(document).on('opened', '#click-to-play-<?php print $id; ?>', function () {
  var e=jQuery('#click-to-play-link-<?php print $id; ?>').attr('data-oembed');
  jQuery('#click-to-play-<?php print $id; ?> .flex-video').html(e);
}).on('close', '#click-to-play-<?php print $id; ?>', function () {
  jQuery('#click-to-play-<?php print $id; ?> .flex-video').html();
});
</script>

<style type="text/css">
.click-to-play-video-wrapper {
  max-width: 900px;
  margin: 0 auto;
}
</style>